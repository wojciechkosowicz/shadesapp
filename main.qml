import QtQuick 2.4
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3

Window {
    visible: true
    id: mywindow
    visibility: Window.FullScreen
    Rectangle {
        anchors.fill: parent
   ColumnLayout {
       Button {
            text: "Get Data"
            onClicked: dataTransporter.sendToDevice()
    }
    Label {
        id: textLabel
            text: "Hello world"
            font.pixelSize: 60
            font.italic: true
            color: "steelblue"

        }
   }
    Connections {
        target: dataTransporter
        onMessageReceived: {
            textLabel.text = dataTransporter.udpMessage;
        }

    }
}
}
