#ifndef UDPDATATRANSPORTER_H
#define UDPDATATRANSPORTER_H

#include <QObject>
#include <QUdpSocket>
#include <datatransporter.h>
#include <QByteArray>
class UDPDataTransporter : public DataTransporter{
    Q_OBJECT
    Q_PROPERTY(QString udpMessage READ udpMessage WRITE setUdpMessage NOTIFY messageReceived)
public:
    explicit UDPDataTransporter(QObject *parent = 0);
    void logger();
    QString udpMessage_;
    QString udpMessage() { return udpMessage_; }
    void setUdpMessage(QString udpMessage) { udpMessage_ = udpMessage;
                                             emit messageReceived(udpMessage);
                                            }
private:
    QUdpSocket* udpSocket;
signals:
    void messageReceived(QString);
public slots:
    virtual void sendToDevice();
};

#endif // UDPDATATRANSPORTER_H
