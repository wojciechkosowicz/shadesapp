#ifndef DATATRANSPORTER_H
#define DATATRANSPORTER_H

#include <QDebug>
#include <QObject>
class DataTransporter : public QObject
{
    Q_OBJECT
public:
    explicit DataTransporter(QObject *parent = 0);

signals:

public slots:
    virtual void sendToDevice() = 0;
    virtual void logger() {
        qDebug() << "hejoo";
    }
};
#endif // DATATRANSPORTER_H
