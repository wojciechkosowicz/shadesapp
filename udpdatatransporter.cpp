#include "udpdatatransporter.h"
#include <QNetworkInterface>
#include <QList>

UDPDataTransporter::UDPDataTransporter(QObject *parent) : DataTransporter(parent)
{
    udpSocket = new QUdpSocket(this);
    udpSocket->bind(QHostAddress::Any, 45454);
    connect(udpSocket, SIGNAL(readyRead()), this, SLOT(logger()));
}
void UDPDataTransporter::sendToDevice() {
    QByteArray datagram = "Asking for data";
    udpSocket->writeDatagram(datagram.data(), datagram.size(),
                             QHostAddress::Broadcast, 45454);
}

void UDPDataTransporter::logger() {
while (udpSocket->hasPendingDatagrams()) {
  QHostAddress sender;
  quint16 senderPort;
  QByteArray buf(udpSocket->pendingDatagramSize(), Qt::Uninitialized);
  udpSocket->readDatagram(buf.data(), buf.size(), &sender, &senderPort);
  qDebug() << "Message from: " << sender;
  qDebug() << "Message port: " << senderPort;
  qDebug() << "Message: " << buf;
  udpMessage_ = QString(buf);
  emit messageReceived(QString(buf));
}
}
