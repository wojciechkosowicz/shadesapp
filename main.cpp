
#include <QGuiApplication>
#include <QtQml/QQmlEngine>
#include <QtQuick/QQuickView>
#include <QDebug>
#include <QQmlContext>
#include <QStandardPaths>
#include <datatransporter.h>
#include <udpdatatransporter.h>

int main(int argc, char *argv[])
{
    QGuiApplication application(argc, argv);
    QQuickView view;
    DataTransporter* dataTransporter = new UDPDataTransporter();
    view.rootContext()->setContextProperty("dataTransporter", QVariant::fromValue(dataTransporter));
    view.setSource(QUrl(QStringLiteral("qrc:/main.qml")));
    view.setResizeMode(QQuickView::SizeRootObjectToView);
    QObject::connect(view.engine(), SIGNAL(quit()), qApp, SLOT(quit()));

    return application.exec();
}

